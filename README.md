# DLTxLibs


## Versions

#### 1.0.0

- TXLiteAVSDK_UGC 5.3.6004
- ImSDK 3.3.2.13987.13861

#### 1.0.1

- TXLiteAVSDK_UGC 5.4.6097
- ImSDK 3.3.2.13987.13861

#### 1.0.2

- remove libs

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

DLTxLibs is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'DLTxLibs'
```

## Author

Luo Yu, indie.luo@gmail.com

## License

DLTxLibs is available under the MIT license. See the LICENSE file for more info.
