//
//  DLAppDelegate.h
//  DLTxLibs
//
//  Created by Luo Yu on 11/25/2018.
//  Copyright (c) 2018 Luo Yu. All rights reserved.
//

@import UIKit;

@interface DLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
