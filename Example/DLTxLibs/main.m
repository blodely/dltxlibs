//
//  main.m
//  DLTxLibs
//
//  Created by Luo Yu on 11/25/2018.
//  Copyright (c) 2018 Luo Yu. All rights reserved.
//

@import UIKit;
#import "DLAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([DLAppDelegate class]));
    }
}
