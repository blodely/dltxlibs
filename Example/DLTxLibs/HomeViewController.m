//
//  HomeViewController.m
//  DLTxLibs_Example
//
//  Created by Luo Yu on 2018/11/26.
//  Copyright © 2018 Luo Yu. All rights reserved.
//

#import "HomeViewController.h"
#import <Masonry/Masonry.h>

#import <TXLiteAVSDK_UGC/TXLiteAVSDK.h>
#import <ImSDK/ImSDK.h>
#import <TLSSDK/TLSHelper.h>


@interface HomeViewController () {
	
	__weak UILabel *lblTxLiteAVSDK_UGC;
	__weak UILabel *lblImSDK;
	__weak UILabel *lblTLSSDK;
}

@end

@implementation HomeViewController

- (void)loadView {
	[super loadView];
	
	self.navigationItem.title = @"SDK Packed by L.Y.";
	
	CGFloat padding = 15;
	
	{
		// MARK: LABEL TX LITE AV SDK UGC
		UILabel *label = [[UILabel alloc] init];
		label.textAlignment = NSTextAlignmentCenter;
		[self.view addSubview:label];
		lblTxLiteAVSDK_UGC = label;
		
		[label mas_makeConstraints:^(MASConstraintMaker *make) {
			if (@available(iOS 11.0, *)) {
				make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop).offset(padding);
			} else {
				make.top.equalTo(self.view).offset(padding);
			}
			make.left.right.equalTo(self.view);
		}];
	}
	
	{
		// MARK: LABEL ImSDK
		UILabel *label = [[UILabel alloc] init];
		label.textAlignment = NSTextAlignmentCenter;
		[self.view addSubview:label];
		lblImSDK = label;
		
		[label mas_makeConstraints:^(MASConstraintMaker *make) {
			make.top.equalTo(self->lblTxLiteAVSDK_UGC.mas_bottom).offset(padding);
			make.left.right.equalTo(self.view);
		}];
	}
	
	{
		// MARK: LABEL TLSSDK
		UILabel *label = [[UILabel alloc] init];
		label.textAlignment = NSTextAlignmentCenter;
		[self.view addSubview:label];
		lblTLSSDK = label;
		
		[label mas_makeConstraints:^(MASConstraintMaker *make) {
			make.top.equalTo(self->lblImSDK.mas_bottom).offset(padding);
			make.left.right.equalTo(self.view);
		}];
	}
}

- (void)viewDidLoad {
	[super viewDidLoad];
	// DO ANY ADDITIONAL SETUP AFTER LOADING THE VIEW.
	
	lblTxLiteAVSDK_UGC.text = [NSString stringWithFormat:@"TXLiteAVSDK_UGC = %@", [TXLiveBase getSDKVersionStr]];
	lblImSDK.text = [NSString stringWithFormat:@"ImSDK = %@", [[TIMManager sharedInstance] GetVersion]];
	lblTLSSDK.text = [NSString stringWithFormat:@"TLSSDK = %@", [[TLSHelper getInstance] getSDKVersion]];
}

@end
